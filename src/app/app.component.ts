import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'wind-power';

  stationId: string;
  subscriptionKey: string;

  stationData: any;

  constructor(
    private http: HttpClient
  ) {}

  initData() {
    this.stationId = 'fcbbec3d-4245-e911-867d-0003ff5960dd';
    this.subscriptionKey = 'someKey';
  }

  ngOnInit() {
    this.initData();
    // this.getStationData();
  }

  getStationData() {
    // tslint:disable-next-line:curly
    if (!this.stationId) return;
    // tslint:disable-next-line:curly
    if (!this.subscriptionKey) return;

    this.http.get(
      `https://apimgmt.www.wow.metoffice.gov.uk/api/observations/bysite`,
      {
        params: {
          site_id: this.stationId,
          start_time: this.dateString( this.yearAgo(new Date()) ),
          end_time: this.dateString( new Date() )
        },
        headers: {
          'Ocp-Apim-Subscription-Key': this.subscriptionKey
        }
      }
    ).pipe( tap( x => console.log(x) ) )
    .subscribe(
      next => this.stationData = next,
      err => this.stationData = err
    );
  }

  yearAgo(date: Date): Date {
    return new Date(
      date.getFullYear() - 1,
      date.getMonth(),
      date.getDay(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
      date.getMilliseconds()
    );
  }

  dateString(date: Date): string {
    return '' + date.toISOString();
  }
}
